// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::Command;
use std::process::Stdio;

use std::io::Write;

mod commands;
use crate::commands::*;
use crate::ffi::GPGKey;

use walkdir::WalkDir;

struct PasswordStore {
    path: String,
}

fn password_store_from_directory(path: String) -> Box<PasswordStore> {
    Box::from(PasswordStore { path })
}

fn list_gpg_keys() -> Result<Vec<ffi::GPGKey>, String> {
    let stdout = run_command("gpg", vec!["--list-secret-keys"])?;

    fn find_key_name(lines: &mut std::str::Split<'_, char>) -> Option<String> {
        lines
            .find(|line| line.starts_with("uid"))
            .map(|o| o.to_string())
    }

    fn find_key_id(lines: &mut std::str::Split<'_, char>) -> Option<String> {
        lines
            .find(|line| line.starts_with("      "))
            .map(|o| o.trim().to_string())
    }

    Ok(stdout
        .split("\n\n")
        .filter_map(|block| {
            let mut lines = block.split('\n');
            let uid = find_key_name(&mut lines.clone());
            let id = find_key_id(&mut lines);

            if let Some(uid) = uid {
                if let Some(id) = id {
                    return Some(GPGKey {
		        uid,
			id,
                    });
                }
            }

            None
        })
        .collect())
}

fn create_password_store(path: String, key: &str, passphrase: &str) {
    let store = Box::from(PasswordStore { path });
    drop(store.run_authenticated_pass_command(passphrase, "pass", &["init", key]));
    drop(store.run_authenticated_pass_command(passphrase, "pass", &["git", "init"]));
}

impl PasswordStore {
    fn passwords(&self) -> Vec<String> {
        let mut ret: Vec<String> = WalkDir::new(&self.path)
            .into_iter()
            .filter_entry(|e| e.file_name().to_str().map(|s| s != ".git").unwrap_or(true))
            .filter_map(|e| e.ok())
            .filter(|entry| {
                entry
                    .file_name()
                    .to_str()
                    .map(|s| !s.starts_with('.'))
                    .unwrap_or(true)
            })
            .filter(|entry| !entry.file_type().is_dir())
            .map(|entry| entry.into_path())
            .map(|name| name.to_string_lossy().to_string())
            .filter(|name| !name.starts_with('.'))
            .map(|name| name.replace(".gpg", ""))
            .filter_map(|name| name.strip_prefix(&self.path).map(|s| s.to_string()))
            .collect();

        ret.sort();
        ret
    }

    fn run_pass_command(&self, executable: &str, arguments: Vec<&str>) -> CommandResult {
        io_result_to_command_result(
            Command::new(executable)
                .args(arguments)
                .env("PASSWORD_STORE_DIR", &self.path)
                .output(),
        )
    }

    fn get_gpg_args(passphrase: &str) -> String {
        format!(
            "--pinentry-mode loopback --batch --passphrase {}",
            passphrase
        )
    }

    fn run_authenticated_pass_command(
        &self,
        passphrase: &str,
        executable: &str,
        arguments: &[&str],
    ) -> CommandResult {
        io_result_to_command_result(
            Command::new(executable)
                .args(arguments)
                .env("PASSWORD_STORE_DIR", &self.path)
                .env(
                    "PASSWORD_STORE_GPG_OPTS",
                    PasswordStore::get_gpg_args(passphrase),
                )
                .output(),
        )
    }

    fn copy_to_clipboard(&self, name: &str, passphrase: &str) -> CommandResult {
        self.run_authenticated_pass_command(passphrase, "pass", &["-c", name])
    }

    fn generate(&self, name: &str, length: i32, symbols: bool, passphrase: &str) -> CommandResult {
        let length = length.to_string();
        let mut args = vec!["generate", name, &length];

        if !symbols {
            args.push("--no-symbols")
        }

        self.run_authenticated_pass_command(passphrase, "pass", &args)
    }

    fn insert(&self, name: &str, password: &str, passphrase: &str) -> CommandResult {
        match Command::new("pass")
            .args(vec!["insert", "--echo", name])
            .env("PASSWORD_STORE_DIR", &self.path)
            .env(
                "PASSWORD_STORE_GPG_OPTS",
                PasswordStore::get_gpg_args(passphrase),
            )
            .stdin(Stdio::piped())
            .spawn()
        {
            Ok(mut child) => {
                let stdin = child.stdin.as_mut().expect("Failed to open stdin");
                stdin
                    .write_all(password.as_bytes())
                    .expect("Failed to write to stdin");
                io_result_to_command_result(child.wait_with_output())
            }
            Err(e) => CommandResult::Err(format!("{}", e)),
        }
    }

    fn remove(&self, name: &str) -> CommandResult {
        self.run_pass_command("pass", vec!["rm", name])
    }
}

#[cxx::bridge]
mod ffi {
    struct GPGKey {
        uid: String,
        id: String,
    }

    extern "Rust" {
        type PasswordStore;

        fn password_store_from_directory(path: String) -> Box<PasswordStore>;
        fn create_password_store(path: String, key: &str, passphrase: &str);
        fn list_gpg_keys() -> Result<Vec<GPGKey>>;

        fn passwords(self: &PasswordStore) -> Vec<String>;
        fn copy_to_clipboard(&self, name: &str, passphrase: &str) -> Result<String>;
        fn generate(
            self: &PasswordStore,
            name: &str,
            length: i32,
            symbols: bool,
            passphrase: &str,
        ) -> Result<String>;
        fn insert(
            self: &PasswordStore,
            name: &str,
            password: &str,
            passphrase: &str,
        ) -> Result<String>;
        fn remove(&self, name: &str) -> Result<String>;
    }
}
