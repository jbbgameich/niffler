// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::io::Result as IOResult;
use std::process::Command;
use std::process::Output;

pub type CommandResult = Result<String, String>;

pub fn io_result_to_command_result(result: IOResult<Output>) -> CommandResult {
    match result {
        Ok(o) => {
            if o.status.success() {
                CommandResult::Ok(String::from_utf8_lossy(&o.stdout).to_string())
            } else {
                CommandResult::Err(String::from_utf8_lossy(&o.stderr).to_string())
            }
        }
        Err(e) => CommandResult::Err(format!("{}", e)),
    }
}

pub fn run_command(executable: &str, arguments: Vec<&str>) -> CommandResult {
    io_result_to_command_result(Command::new(executable).args(arguments).output())
}
