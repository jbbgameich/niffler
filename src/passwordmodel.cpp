// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "passwordmodel.h"

#include <QtConcurrent>
#include <QClipboard>
#include <QGuiApplication>
#include <QDir>

#include "utils.h"

#include <chrono>

using namespace std::chrono_literals;

PasswordModel::PasswordModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_copiedIndex(-1)
    , m_passwordStore(password_store_from_directory(Utils::passwordStoreDir().toStdString()))
{
    connect(&m_passwordsWatcher, &QFutureWatcher<rust::Vec<rust::String>>::finished, this, [&] {
        beginResetModel();
        m_passwords = m_passwordsWatcher.result();
        endResetModel();
        Q_EMIT countChanged();
    });

    reloadPasswords();

    connect(&m_generateWatcher, &QFutureWatcher<rust::String>::finished, this, [&] {
        const auto password = m_generateWatcher.result();
        Q_EMIT generatingFinished(QString::fromStdString({password.begin(), password.end()}));
    });

    connect(&m_insertWatcher, &QFutureWatcher<rust::String>::finished, this, &PasswordModel::insertFinished);
    connect(&m_insertWatcher, &QFutureWatcher<void>::finished, this, &PasswordModel::reloadPasswords);
    connect(&m_generateWatcher, &QFutureWatcher<void>::finished, this, &PasswordModel::reloadPasswords);
    connect(&m_removeWatcher, &QFutureWatcher<void>::finished, this, &PasswordModel::reloadPasswords);
}

QHash<int, QByteArray> PasswordModel::roleNames() const
{
    return {
        {Qt::DisplayRole, "name"},
        {Role::Copied, "copied"}
    };
}

QVariant PasswordModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        return QString::fromStdString(std::string(m_passwords[index.row()]));
    case Role::Copied:
        return (m_copiedIndex == index.row());
    }

    return {};
}

int PasswordModel::rowCount(const QModelIndex &index) const
{
    return index.isValid() ? 0 : m_passwords.size();
}

void PasswordModel::copyToClipboard(const QString &name, const QString &passphrase)
{
    auto it = std::find_if(m_passwords.begin(), m_passwords.end(), [&](const rust::String &passwordName) {
        return name.toStdString() == std::string(passwordName);
    });
    int index = std::distance(m_passwords.begin(), it);
    m_copiedIndex = index;
    QModelIndex modelIndex = this->index(index);
    Q_EMIT dataChanged(modelIndex, modelIndex, {Role::Copied});
    m_copiedIndex = -1;
    Q_EMIT dataChanged(modelIndex, modelIndex, {Role::Copied});

    Q_EMIT copiedToClipboard();

    m_copyToClipboard.setFuture(QtConcurrent::run([=] {
        try {
            return m_passwordStore->copy_to_clipboard(name.toStdString(), passphrase.toStdString());
        } catch (const std::exception &e) {
            Q_EMIT errorOccured(QString::fromUtf8(e.what()));
            return rust::String();
        }
    }));
}

void PasswordModel::generate(const QString &name, int length, bool symbols, const QString &passphrase)
{
    m_generateWatcher.setFuture(QtConcurrent::run([=] {
        try {
            return m_passwordStore->generate(name.toStdString(), length, symbols, passphrase.toStdString());
        } catch (const std::exception &e) {
            Q_EMIT errorOccured(QString::fromUtf8(e.what()));
            return rust::String();
        }
    }));
}

void PasswordModel::insert(const QString &name, const QString &password, const QString &passphrase)
{
    m_insertWatcher.setFuture(QtConcurrent::run([=] {
        try {
            return m_passwordStore->insert(name.toStdString(), password.toStdString(), passphrase.toStdString());
        } catch (const std::exception &e) {
            Q_EMIT errorOccured(QString::fromUtf8(e.what()));
            return rust::String();
        }
    }));
}

void PasswordModel::remove(const QString &name)
{
    m_removeWatcher.setFuture(QtConcurrent::run([=] {
        try {
            m_passwordStore->remove(name.toStdString());
        } catch (const std::exception &e) {
            Q_EMIT errorOccured(QString::fromUtf8(e.what()));
        }
    }));
}

void PasswordModel::reloadPasswords() {
    m_passwordsWatcher.setFuture(QtConcurrent::run([=] {
        try {
            return m_passwordStore->passwords();
        } catch (const std::exception &e) {
            Q_EMIT errorOccured(QString::fromUtf8(e.what()));
            return rust::Vec<rust::String>();
        }
    }));
}
