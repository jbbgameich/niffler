// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.0
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.5 as Controls
import org.kde.kirigami 2.12 as Kirigami

import org.kde.niffler 1.0

Kirigami.OverlaySheet {
    property PasswordModel model

    header: Kirigami.Heading {
        text: i18n("Generate new password")
    }

    contentItem: Kirigami.FormLayout {
        Controls.TextField {
            id: nameField
            Kirigami.FormData.label: i18n("Username / E-Mail-Address")
        }

        Controls.TextField {
            id: domainField
            Kirigami.FormData.label: i18n("Website")
        }

        RowLayout {
            Kirigami.FormData.label: i18n("Password Length")

            Controls.Slider {
                stepSize: 1
                id: lengthSlider
                from: 10
                value: 20
                to: 300
            }

            Controls.Label {
                text: lengthSlider.value
            }
        }

        Controls.CheckBox {
            id: symbolsCheckbox
            Kirigami.FormData.label: i18n("Include symbols")
            checked: true
        }

        Kirigami.PasswordField {
            id: passphraseField
            Kirigami.FormData.label: i18n("Passphrase")
            onAccepted: generateButton.clicked()
        }
    }

    footer: RowLayout {
        Controls.BusyIndicator {
            id: busyIndicator
        }

        Controls.Button {
            id: generateButton
            text: i18n("Generate")
            enabled: nameField.text
            onClicked: {
                const name = Utils.domainFromUrl(domainField.text) + "/" + nameField.text
                model.generate(name, lengthSlider.value, symbolsCheckbox.checked, passphraseField.text)
                busyIndicator.visible = true
            }
        }
    }

    onSheetOpenChanged: {
        if (sheetOpen) {
            nameField.forceActiveFocus()
        }

        busyIndicator.visible = false
    }
}
