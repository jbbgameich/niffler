// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.0
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.5 as Controls
import org.kde.kirigami 2.12 as Kirigami

import org.kde.niffler 1.0

Kirigami.OverlaySheet {
    property PasswordModel model;

    header: Kirigami.Heading {
        text: i18n("Insert password")
    }

    contentItem: Kirigami.FormLayout {
        Controls.TextField {
            id: nameField
            Kirigami.FormData.label: i18n("Username / E-Mail-Address")
        }

        Controls.TextField {
            id: domainField
            Kirigami.FormData.label: i18n("Website")
        }

        Kirigami.PasswordField {
            id: passwordField
            Kirigami.FormData.label: i18n("Password")
        }

        Kirigami.PasswordField {
            id: passphraseField
            Kirigami.FormData.label: i18n("Passphrase")
            onAccepted: addButton.clicked()
        }
    }

    footer: RowLayout {
        Controls.BusyIndicator {
            id: busyIndicator
        }

        Controls.Button {
            id: addButton
            text: i18n("Add")
            enabled: nameField.text && passwordField.text
            onClicked: {
                const name = Utils.domainFromUrl(domainField.text) + "/" + nameField.text
                model.insert(name, passwordField.text, passphraseField.text)
                busyIndicator.visible = true
            }
        }
    }

    onSheetOpenChanged: {
        if (sheetOpen) {
            nameField.forceActiveFocus()
        }

        busyIndicator.visible = false
    }
}
