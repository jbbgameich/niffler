// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.0
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.5 as Controls
import org.kde.kirigami 2.12 as Kirigami

import org.kde.niffler 1.0

Kirigami.OverlaySheet {
    signal passphraseEntered(string passphrase)

    onSheetOpenChanged: {
        if (sheetOpen) {
            passphraseField.forceActiveFocus()
        }
    }

    header: Kirigami.Heading {
        text: i18n("Enter passphrase")
    }

    contentItem: ColumnLayout {
        Controls.Label {
            text: i18n("Please unlock your key:")

            Layout.fillWidth: true
            wrapMode: Text.WordWrap
        }

        Kirigami.FormLayout {
            Layout.fillWidth: true

            Kirigami.PasswordField {
                id: passphraseField
                echoMode: TextInput.Password
                Kirigami.FormData.label: i18n("Passphrase")

                onAccepted: unlockButton.clicked()
            }
        }
    }

    footer: Controls.Button {
        id: unlockButton
        text: i18n("Unlock")
        enabled: passphraseField.text
        onClicked: {
            passphraseEntered(passphraseField.text)
            passphraseField.text = ""
            close()
        }
    }
}
