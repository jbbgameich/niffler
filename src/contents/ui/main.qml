// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.1
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls

import org.kde.niffler 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("Niffler")

    Loader {
        id: sheetLoader
    }

    pageStack.initialPage: Utils.passwordStoreExists ? "qrc:/PasswordManagerPage.qml" : "qrc:/welcome/WelcomePage.qml"
}
