// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>

import QtQuick 2.7
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.5 as Controls
import org.kde.kirigami 2.5 as Kirigami

import org.kde.niffler 1.0

Kirigami.Page {
    property string keyId
    property string passphrase

    ColumnLayout {
        anchors.centerIn: parent

        Controls.BusyIndicator {
            Layout.fillWidth: true
            width: 50
            height: width
        }
        Kirigami.Heading {
            Layout.fillWidth: true
            text: i18n("Creating the password store…")
        }
    }

    Connections {
        Component.onCompleted: Utils.createPasswordStore(keyId, passphrase)
        target: Utils

        function onPasswordStoreCreated() {
            while (pageStack.layers.depth > 1) {
                pageStack.layers.pop()
            }
        }
    }
}
