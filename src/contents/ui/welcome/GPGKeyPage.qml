// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.7
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.5 as Controls
import org.kde.kirigami 2.5 as Kirigami

import org.kde.niffler 1.0

Kirigami.ScrollablePage {
    id: gpgKeyPage
    title: "GPG Keys"

    property string chosenKey: gpgKeyModel.currentItem ? gpgKeyModel.currentItem.id : null

    ListView {
        header: Controls.Control {
            padding: 10
            contentItem: Kirigami.Heading {
                text: i18n("Select a key to encrypt your passwords with")
            }
        }

        model: GPGKeyModel {
            id: gpgKeyModel
        }
        delegate: Kirigami.BasicListItem {
            id: listItem

            required property string name
            required property string id

            onClicked: gpgKeyPage.chosenKey = listItem.id
            highlighted: chosenKey == listItem.id

            height: 50
            contentItem: ColumnLayout {
                Controls.Label {
                    elide: Qt.ElideRight
                    Layout.fillWidth: true
                    text: listItem.name
                }
                Controls.Label {
                    elide: Qt.ElideRight
                    Layout.fillWidth: true
                    text: listItem.id
                }
            }
        }
    }
    footer: Controls.Button {
        text: i18n("Create")
        enabled: gpgKeyPage.chosenKey
        onClicked: {
            sheetLoader.source = "qrc:/PassphraseSheet.qml"
            sheetLoader.item.passphraseEntered.connect(function(passphrase) {
                pageStack.layers.push("qrc:/welcome/CreatingPage.qml", {
                    "keyId": gpgKeyPage.chosenKey,
                    "passphrase": passphrase
                })
            })
            sheetLoader.item.open()
        }
    }
}
