// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.7
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.7 as Kirigami
import QtQuick.Controls 2.7 as Controls

Kirigami.Page {
    GridLayout {
        anchors.fill: parent

        ColumnLayout {
            Layout.alignment: Qt.AlignHCenter
            Kirigami.Heading {
                text: i18n("Set up a password Store")
            }
            Controls.Label {
                text: i18n("Create a password store to get started")
            }

            Controls.Button {
                Layout.alignment: Qt.AlignHCenter
                text: i18n("Create")
                onClicked: pageStack.layers.push("qrc:/welcome/GPGKeyPage.qml")
            }
        }
    }
}
