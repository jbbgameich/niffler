// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.7
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.5 as Controls
import org.kde.kirigami 2.12 as Kirigami

import org.kde.niffler 1.0

Kirigami.ScrollablePage {
    title: i18n("Passwords")

    property string currentPasswordName
    header: Controls.Control {
        padding: Kirigami.Units.largeSpacing

        contentItem: Kirigami.SearchField {
            id: searchField
        }
    }

    actions.contextualActions: [
        Kirigami.Action {
            id: generateAction
            text: i18n("Generate new")
            icon.name: "password-generate"
            onTriggered: {
                sheetLoader.source = "GenerateSheet.qml"
                sheetLoader.item.model = passwordModel
                sheetLoader.item.open()
            }
        },
        Kirigami.Action {
            text: i18n("Add existing password")
            icon.name: "document-encrypted"
            onTriggered: {
                sheetLoader.source = "InsertSheet.qml"
                sheetLoader.item.model = passwordModel
                sheetLoader.item.open()
            }
        }
    ]

    ListView {
        Kirigami.PlaceholderMessage {
            text: i18n("You don't have any saved passwords yet."
                       + " You can get started by generating a new one.")

            visible: passwordModel.count == 0

            anchors.centerIn: passwordList
            width: parent.width - Kirigami.Units.largeSpacing * 2

            helpfulAction: generateAction
        }

        id: passwordList
        model: PasswordFilter {
            filterString: searchField.text
            onFilterStringChanged: console.log(filterString)

            sourceModel: PasswordModel {
                id: passwordModel

                onGeneratingFinished: {
                    sheetLoader.item.close()
                }
                onInsertFinished: {
                    sheetLoader.item.close()
                }
                onCopiedToClipboard: {
                    showPassiveNotification(i18n("Password copied to clipboard"))
                }
                onErrorOccured: (message) => {
                    showPassiveNotification(message)
                }
            }
        }
        delegate: Kirigami.SwipeListItem {
            id: listItem
            required property bool copied
            onCopiedChanged: {
                progressRect.width = listItem.width
                progressRect.width = 0
            }
            required property string name
            required property int index

            contentItem: ColumnLayout {
                Controls.Label {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: name
                }
                Rectangle {
                    id: progressRect
                    visible: animation.running

                    Behavior on width {
                        NumberAnimation {
                            id: animation
                            duration: 45000

                            from: listItem.width
                            to: 0
                        }
                    }

                    height: 2
                    color: Kirigami.Theme.activeTextColor
                }
            }

            onClicked: {
                sheetLoader.source = "qrc:/PassphraseSheet.qml"
                currentPasswordName = listItem.name
                sheetLoader.item.passphraseEntered.connect(function(passphrase) {
                    passwordModel.copyToClipboard(currentPasswordName, passphrase)
                })
                sheetLoader.item.open()
            }

            actions: Kirigami.Action {
                text: i18n("Delete")
                icon.name: "delete"
                onTriggered: passwordModel.remove(listItem.name)
            }
        }
    }
}
