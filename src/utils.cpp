// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "utils.h"

#include <QDir>
#include <QtConcurrent>

#include <lib.rs.h>

Utils::Utils(QObject *parent)
    : QObject(parent)
{
    connect(&m_createPasswordStoreWatcher, &QFutureWatcher<void>::finished, this, &Utils::passwordStoreCreated);
    connect(&m_createPasswordStoreWatcher, &QFutureWatcher<void>::finished, this, &Utils::passwordStoreExistsChanged);
}

QString Utils::passwordStoreDir()
{
    return QDir::home().path() + QDir::separator() + QStringLiteral(".password-store/");
}

bool Utils::passwordStoreExists()
{
    return QDir(passwordStoreDir()).exists();
}

void Utils::createPasswordStore(const QString &key, const QString &passphrase)
{
    m_createPasswordStoreWatcher.setFuture(QtConcurrent::run(&create_password_store,
                                                             passwordStoreDir().toStdString(),
                                                             key.toStdString(),
                                                             passphrase.toStdString()));
}

QString Utils::domainFromUrl(const QString &url)
{
    return QUrl::fromUserInput(url).host();
}
