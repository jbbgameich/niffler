// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef GPGKEYMODEL_H
#define GPGKEYMODEL_H

#include <QAbstractListModel>
#include <QFutureWatcher>

#include <lib.rs.h>

class GPGKeyModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role {
        Name = Qt::UserRole,
        ID
    };
    Q_ENUM(Role)

    explicit GPGKeyModel(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int) const override;
    int rowCount(const QModelIndex &index) const override;

private:
    QFutureWatcher<rust::Vec<GPGKey>> m_keyWatcher;
};

#endif
