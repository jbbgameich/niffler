// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PASSWORDMODEL_H
#define PASSWORDMODEL_H

#include <QAbstractListModel>
#include <QtConcurrent>

#include "lib.rs.h"

class PasswordModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    enum Role {
        Copied = Qt::UserRole + 1
    };

    explicit PasswordModel(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int) const override;
    int rowCount(const QModelIndex &index) const override;

    Q_INVOKABLE void copyToClipboard(const QString &name, const QString &passphrase);
    Q_INVOKABLE void generate(const QString &name, int length, bool symbols, const QString &passphrase);
    Q_INVOKABLE void insert(const QString &name, const QString &password, const QString &passphrase);
    Q_INVOKABLE void remove(const QString &name);

    Q_SIGNAL void generatingFinished(const QString &password);
    Q_SIGNAL void insertFinished();
    Q_SIGNAL void copiedToClipboard();
    Q_SIGNAL void errorOccured(const QString &error);

    Q_SLOT void reloadPasswords();

    int count() {
        return m_passwords.size();
    }
    Q_SIGNAL void countChanged();

private:
    QFutureWatcher<rust::Vec<rust::String>> m_passwordsWatcher;
    QFutureWatcher<rust::String> m_copyToClipboard;
    QFutureWatcher<rust::String> m_generateWatcher;
    QFutureWatcher<rust::String> m_insertWatcher;
    QFutureWatcher<void> m_removeWatcher;

    int m_copiedIndex;

    rust::Vec<rust::String> m_passwords;
    rust::Box<PasswordStore> m_passwordStore;
};

#endif // PASSWORDMODEL_H
