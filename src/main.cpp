// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>
#include <QSortFilterProxyModel>

#include "passwordmodel.h"
#include "gpgkeymodel.h"
#include "utils.h"

class PasswordsFilter : public QSortFilterProxyModel {
    Q_OBJECT

public:
    Q_PROPERTY(QString filterString WRITE setFilterString READ filterString NOTIFY filterStringChanged)

    [[nodiscard]] bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override {
        QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);

        return sourceModel()->data(index, Qt::DisplayRole).toString().contains(m_filterString);
    }

    void setFilterString(const QString &filter) {
        m_filterString = filter;
        Q_EMIT invalidateFilter();
        Q_EMIT filterStringChanged();
    }
    QString filterString() {
        return m_filterString;
    }
    Q_SIGNAL void filterStringChanged();

private:
    QString m_filterString;
};

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("kde.org"));
    QCoreApplication::setApplicationName(QStringLiteral("Niffler"));

    QQmlApplicationEngine engine;

    qmlRegisterAnonymousType<QAbstractItemModel>("org.kde.niffler", 1);
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    qmlRegisterType<PasswordModel>("org.kde.niffler", 1, 0, "PasswordModel");
    qmlRegisterType<PasswordsFilter>("org.kde.niffler", 1, 0, "PasswordFilter");
    qmlRegisterType<GPGKeyModel>("org.kde.niffler", 1, 0, "GPGKeyModel");
    qmlRegisterSingletonType<Utils>("org.kde.niffler", 1, 0, "Utils", [](QQmlEngine *, QJSEngine *) {
        return new Utils();
    });

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }


    return app.exec();
}

#include "main.moc"
