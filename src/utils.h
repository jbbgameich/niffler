// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QObject>
#include <QFutureWatcher>

class Utils : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool passwordStoreExists READ passwordStoreExists NOTIFY passwordStoreExistsChanged)

public:
    Utils(QObject *parent = nullptr);

    static QString passwordStoreDir();
    static bool passwordStoreExists();
    Q_INVOKABLE void createPasswordStore(const QString &key, const QString &passphrase);

    Q_SIGNAL void passwordStoreCreated();
    Q_SIGNAL void passwordStoreExistsChanged();

    Q_INVOKABLE QString domainFromUrl(const QString &url);

private:
    QFutureWatcher<void> m_createPasswordStoreWatcher;
};

#endif // UTILS_H
