// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gpgkeymodel.h"

#include <QtConcurrent>

GPGKeyModel::GPGKeyModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_keyWatcher.setFuture(QtConcurrent::run(&list_gpg_keys));
    connect(&m_keyWatcher, &QFutureWatcher<rust::Vec<rust::String>>::finished, this, [&] {
        beginResetModel();
        endResetModel();
    });
}

QHash<int, QByteArray> GPGKeyModel::roleNames() const
{
    return {
        {Name, "name"},
        {ID, "id"}
    };
}

QVariant GPGKeyModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < int(m_keyWatcher.result().size())) {
        const auto key = m_keyWatcher.result().at(index.row());
        switch (role) {
        case Name:
            return QString::fromStdString({key.uid.begin(), key.uid.end()});
        case ID:
            return QString::fromStdString({key.id.begin(), key.id.end()});
        }
    }
    return {};
}

int GPGKeyModel::rowCount(const QModelIndex &index) const
{
    return index.isValid() ? 0 : m_keyWatcher.result().size();
}
